# INORA
INORA2 is an open-source tool written in TypeScript + Vue, supporting software architects in quick analysis of interaction models and choreographies.

## Release date
The software will be released in April 2023. If you are interested in INORA2, please stay up to date with this repository.
